#!/bin/bash
set -euo pipefail

update-alternatives --set iptables /usr/sbin/iptables-legacy

apt-get update
apt-get install --no-install-recommends -y podman

wipefs -a /dev/md/* || true
(ls /dev/md/* | xargs -n1 mdadm --stop) || true
wipefs -a /dev/nvme0n1p* || true
wipefs -a /dev/nvme1n1p* || true
wipefs -a /dev/nvme0n1
wipefs -a /dev/nvme1n1
wipefs -a /dev/sda
wipefs -a /dev/sdb

podman run \
    --pull=always \
    --privileged \
    --rm \
    -v /dev:/dev \
    -v /run/udev:/run/udev \
    -v .:/data \
    -w /data \
    quay.io/coreos/coreos-installer:release \
    install \
    --ignition-file /data/ignition.ign \
    --platform metal \
    --delete-karg mitigations=auto,nosmt \
    --append-karg mitigations=auto \
    --append-karg selinux=0 \
    -- \
    /dev/nvme0n1

mkdir -p /mnt/boot

mount /dev/nvme0n1p2 /mnt/boot
rm /mnt/boot/EFI/fedora/BOOTX64.CSV
umount /mnt/boot || true

lsblk
systemctl reboot
